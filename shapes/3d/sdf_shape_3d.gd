class_name SdfShape3D
extends Node3D

enum ShapeType3D {
	NONE,
	SPHERE,
	PLANE
}

@export var shape_material: Shape3DMaterial;

var shape_type: ShapeType3D;
var data0: float;
var data1: float;
var data2: float;
var data3: float;
var data4: float;
var data5: float;
var data6: float;
var data7: float;


func _ready() -> void:
	reset_shape_params()


func _process(_delta) -> void:
	reset_shape_params()


func reset_shape_params() -> void:
	shape_type = ShapeType3D.NONE;
	data0 = 0;
	data1 = 0;
	data2 = 0;
	data3 = 0;
	data4 = 0;
	data5 = 0;
	data6 = 0;
	data7 = 0;
