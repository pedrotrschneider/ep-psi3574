class_name Plane3D
extends SdfShape3D


func reset_shape_params() -> void:
	shape_type = ShapeType3D.PLANE;
	data0 = 0;
	data1 = 0;
	data2 = 0;
	data3 = 0;
	data4 = 0;
	data5 = 0;
	data6 = 0;
	data7 = 0;
