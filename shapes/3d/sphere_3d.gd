class_name Sphere3D
extends SdfShape3D

@export var radius: float;


func reset_shape_params() -> void:
	shape_type = ShapeType3D.SPHERE;
	data0 = radius;
	data1 = 0;
	data2 = 0;
	data3 = 0;
	data4 = 0;
	data5 = 0;
	data6 = 0;
	data7 = 0;
