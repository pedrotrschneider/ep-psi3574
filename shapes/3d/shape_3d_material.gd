class_name Shape3DMaterial
extends Resource

@export var color: Color;
@export var texture: Texture2D;
