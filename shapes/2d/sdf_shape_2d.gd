class_name SdfShape2D
extends Node2D

enum ShapeType2D {
	NONE,
	CIRCLE,
	BOX,
	ROUNDED_BOX,
	ORIENTED_BOX,
	SEGMENT,
	THICK_SEGMENT,
	RHOMBUS,
	TRAPEZOID,
	PARALLELLOGRAM,
	EQUILATERAL_TRIANGLE,
	ISOCELES_TRIANGLE,
	TRIANGLE,
	UNEVEN_CAPSULE,
	PENTAGON,
	HEXAGON,
	NGON,
	STAR,
	PIE,
	ARC,
	HORSESHOE,
	VESICA,
	MOON,
	ROUNDED_CROSS,
	EGG,
	HEART,
	CROSS,
	POLYGON,
	ELLIPSE,
	PARABOLA,
	PARABOLA_SEGMENT,
	BEZIER,
	BLOBBY_CROSS,
	TUNNEL,
	STARIS,
	QUADRATIC_CIRCLE,
	HYPERBOLA,
	COOL_S,
	CIRCLE_WAVE,
	ARROW,
	CIRCLE_JOINT,
	FLAT_JOINT,
	HYPERBOLIC_CROSS,
	SPIRAL,
}

@export var shape_material: Shape2DMaterial;

var shape_type: ShapeType2D;
var data0: float;
var data1: float;
var data2: float;
var data3: float;
var data4: float;
var data5: float;
var data6: float;
var data7: float;


func _ready() -> void:
	reset_shape_params()


func _process(_delta) -> void:
	reset_shape_params()


func reset_shape_params() -> void:
	shape_type = ShapeType2D.NONE;
	data0 = 0;
	data1 = 0;
	data2 = 0;
	data3 = 0;
	data4 = 0;
	data5 = 0;
	data6 = 0;
	data7 = 0;
