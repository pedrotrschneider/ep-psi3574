class_name Box2D
extends SdfShape2D

@export var width: float;
@export var height: float;


func reset_shape_params() -> void:
	self.shape_type = ShapeType2D.BOX;
	self.data0 = width;
	self.data1 = height;
	self.data2 = 0;
	self.data3 = 0;
	self.data4 = 0;
	self.data5 = 0;
	self.data6 = 0;
	self.data7 = 0;
