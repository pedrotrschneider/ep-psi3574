class_name Circle2D
extends SdfShape2D

@export var radius: float;


func reset_shape_params() -> void:
	self.shape_type = ShapeType2D.CIRCLE;
	self.data0 = radius;
	self.data1 = 0;
	self.data2 = 0;
	self.data3 = 0;
	self.data4 = 0;
	self.data5 = 0;
	self.data6 = 0;
	self.data7 = 0;
