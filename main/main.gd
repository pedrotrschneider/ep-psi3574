extends Node2D

@export var camera_position: Node3D;
@export var camera_target: Node3D;
@export var camera_pivot: Node3D;

@export var sun_shape: SdfShape3D;
@export var mercury_shape: SdfShape3D;
@export var venus_shape: SdfShape3D;
@export var earth_shape: SdfShape3D;
@export var mars_shape: SdfShape3D;
@export var jupiter_shape: SdfShape3D;
@export var saturn_shape: SdfShape3D;
@export var uranus_shape: SdfShape3D;
@export var neptune_shape: SdfShape3D;

@export var skybox_texture: Texture2D;

@export var shape_manager_2d: ShapeManager2D;
@export var material_2d: ShaderMaterial;

@export var shape_manager_3d: ShapeManager3D;
@export var material_3d: ShaderMaterial;

var planet_shapes = [];
var focused_shape: Sphere3D;

var orbital_params := {
	"sun_shape":     { "semi_major_axis": 0.0, "semi_minor_axis": 0.0,       "orbital_speed": 0.0,  "angle": 0.0      },
	"mercury_shape": { "semi_major_axis": 1.0, "semi_minor_axis": 0.9,       "orbital_speed": 1.0,  "angle": PI / 4.0 },
	"venus_shape":   { "semi_major_axis": 1.5, "semi_minor_axis": 1.1 * 1.5, "orbital_speed": 0.6,  "angle": PI / 3.0 },
	"earth_shape":   { "semi_major_axis": 2.6, "semi_minor_axis": 0.9 * 2.6, "orbital_speed": 0.3,  "angle": PI / 2.0 },
	"mars_shape":    { "semi_major_axis": 3.2, "semi_minor_axis": 1.1 * 3.2, "orbital_speed": 0.17, "angle": PI       },
	"jupiter_shape": { "semi_major_axis": 4.0, "semi_minor_axis": 0.9 * 4.0, "orbital_speed": 0.12, "angle": PI * 1.5 },
	"saturn_shape":  { "semi_major_axis": 4.8, "semi_minor_axis": 1.1 * 4.8, "orbital_speed": 0.08, "angle": PI * 2.0 },
	"uranus_shape":  { "semi_major_axis": 5.4, "semi_minor_axis": 0.9 * 5.4, "orbital_speed": 0.06, "angle": PI * 3.5 },
	"neptune_shape": { "semi_major_axis": 6.0, "semi_minor_axis": 1.1 * 6.0, "orbital_speed": 0.05, "angle": PI * 3.0 },
};

func _ready():
	shape_manager_2d.material_2d = material_2d;
	shape_manager_3d.material_3d = material_3d;
	
	planet_shapes.append(sun_shape);
	planet_shapes.append(mercury_shape);
	planet_shapes.append(venus_shape);
	planet_shapes.append(earth_shape);
	planet_shapes.append(mars_shape);
	planet_shapes.append(jupiter_shape);
	planet_shapes.append(saturn_shape);
	planet_shapes.append(uranus_shape);
	planet_shapes.append(neptune_shape);
	
	focused_shape = sun_shape;
	

func _physics_process(delta):
	material_3d.set_shader_parameter("skybox_texture", skybox_texture);
	
	material_3d.set_shader_parameter("camera_position", camera_position.global_position);
	material_3d.set_shader_parameter("camera_target", camera_target.global_position);
	
	if Input.is_action_pressed("ui_up"):
		camera_position.position.y += 0.02 * 60 * delta;
	if Input.is_action_pressed("ui_down"):
		camera_position.position.y -= 0.02 * 60 * delta;
	if Input.is_action_pressed("ui_left"):
		camera_pivot.rotation_degrees.y -= 0.5 * 60 * delta;
	if Input.is_action_pressed("ui_right"):
		camera_pivot.rotation_degrees.y += 0.5 * 60 * delta;
	camera_position.position.y = max(camera_position.position.y, 0);
	
	if focused_shape:
		camera_target.position = lerp(camera_target.position, focused_shape.global_position, delta * 10);
		camera_pivot.position = lerp(camera_pivot.position, focused_shape.global_position, delta * 10);
		camera_position.position.z = lerp(camera_position.position.z, focused_shape.radius * 2, delta * 10);
	
	var i = 0;
	for planet_name in orbital_params.keys():
		var planet_data = orbital_params[planet_name];
		var x = planet_data["semi_major_axis"] * cos(planet_data["angle"]);
		var z = planet_data["semi_minor_axis"] * sin(planet_data["angle"]);
		
		planet_shapes[i].position = Vector3(x, 0, z);
		planet_data["angle"] += delta * planet_data["orbital_speed"];
		
		i += 1;


func _input(event):
	if event is InputEventKey and event.pressed:
		if event.keycode == KEY_1:
			focus_shape(sun_shape);
		elif event.keycode == KEY_2:
			focus_shape(mercury_shape);
		elif event.keycode == KEY_3:
			focus_shape(venus_shape);
		elif event.keycode == KEY_4:
			focus_shape(earth_shape);
		elif event.keycode == KEY_5:
			focus_shape(mars_shape);
		elif event.keycode == KEY_6:
			focus_shape(jupiter_shape);
		elif event.keycode == KEY_7:
			focus_shape(saturn_shape);
		elif event.keycode == KEY_8:
			focus_shape(uranus_shape);
		elif event.keycode == KEY_9:
			focus_shape(neptune_shape);
	elif event is InputEventMouseMotion:
		if Input.is_action_pressed("mouse_left_click"):
			camera_pivot.rotation_degrees.y -= event.relative.x * 0.5;
			camera_position.rotation_degrees.x += event.relative.y * 0.01;


func focus_shape(shape: Sphere3D):
	focused_shape = shape;
