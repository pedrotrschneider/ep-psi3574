class_name ShapeManager3D
extends Node3D

const MAX_SHAPES: int = 10;

# SDF Material
var material_3d: ShaderMaterial;

var shape_count: int;

# Shape types
var shape_types: Array[int];
# Shape transforms
var shape_transform_positions: Array[Vector3];
var shape_transform_scales: Array[float];
var shape_transform_rotations: Array[Vector3];
# Shape material
var shape_material_colors: Array[Color];
var shape_material_textures: Array[Texture2D];
# Shape data
var shape_data0: Array[float];
var shape_data1: Array[float];
var shape_data2: Array[float];
var shape_data3: Array[float];
var shape_data4: Array[float];
var shape_data5: Array[float];
var shape_data6: Array[float];
var shape_data7: Array[float];


func _ready():
	shape_types.resize(MAX_SHAPES);
	
	shape_transform_positions.resize(MAX_SHAPES);
	shape_transform_scales.resize(MAX_SHAPES);
	shape_transform_rotations.resize(MAX_SHAPES);
	
	shape_material_colors.resize(MAX_SHAPES);
	shape_material_textures.resize(MAX_SHAPES);
	
	shape_data0.resize(MAX_SHAPES);
	shape_data1.resize(MAX_SHAPES);
	shape_data2.resize(MAX_SHAPES);
	shape_data3.resize(MAX_SHAPES);
	shape_data4.resize(MAX_SHAPES);
	shape_data5.resize(MAX_SHAPES);
	shape_data6.resize(MAX_SHAPES);
	shape_data7.resize(MAX_SHAPES);


func _process(_delta):
	reset_shader_params();
	
	shape_count = 0;
	
	for i in self.get_child_count():
		var child := self.get_child(i) as SdfShape3D;
		if child == null or not child.visible: continue;
		
		shape_count += 1;
		
		shape_types[i] = child.shape_type;
		
		shape_material_colors[i] = child.shape_material.color;
		shape_material_textures[i] = child.shape_material.texture;
		
		shape_transform_positions[i] = child.global_position;
		shape_transform_rotations[i] = child.global_rotation;
		shape_transform_scales[i] = child.scale.x;
		
		shape_data0[i] = child.data0;
		shape_data1[i] = child.data1;
		shape_data2[i] = child.data2;
		shape_data3[i] = child.data3;
		shape_data4[i] = child.data4;
		shape_data5[i] = child.data5;
		shape_data6[i] = child.data6;
		shape_data7[i] = child.data7;
	
	set_shader_uniforms();


func set_shader_uniforms() -> void:
	material_3d.set_shader_parameter("shape_count", shape_count);
	
	material_3d.set_shader_parameter("shape_types", shape_types);
	
	material_3d.set_shader_parameter("shape_material_colors", shape_material_colors);
	material_3d.set_shader_parameter("shape_material_textures", shape_material_textures);
	
	material_3d.set_shader_parameter("shape_transform_positions", shape_transform_positions);
	material_3d.set_shader_parameter("shape_transform_rotations", shape_transform_rotations);
	material_3d.set_shader_parameter("shape_transform_scales", shape_transform_scales);
	
	material_3d.set_shader_parameter("shape_data0", shape_data0);
	material_3d.set_shader_parameter("shape_data1", shape_data1);
	material_3d.set_shader_parameter("shape_data2", shape_data2);
	material_3d.set_shader_parameter("shape_data3", shape_data3);
	material_3d.set_shader_parameter("shape_data4", shape_data4);
	material_3d.set_shader_parameter("shape_data5", shape_data5);
	material_3d.set_shader_parameter("shape_data6", shape_data6);
	material_3d.set_shader_parameter("shape_data7", shape_data7);


func reset_shader_params():
	for i in MAX_SHAPES:
		# Shape type
		shape_types[i] = 0;
		# Shape transform
		shape_transform_positions[i] = Vector3.ZERO;
		shape_transform_scales[i] = 1;
		shape_transform_rotations[i] = Vector3.ZERO;
		# Shape material
		shape_material_colors[i] = Color.WHITE;
		# Shape data
		shape_data0[i] = 0;
		shape_data1[i] = 0;
		shape_data2[i] = 0;
		shape_data3[i] = 0;
		shape_data4[i] = 0;
		shape_data5[i] = 0;
		shape_data6[i] = 0;
		shape_data7[i] = 0;
