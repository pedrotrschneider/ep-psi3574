class_name ShapeManager2D
extends Node2D

const MAX_SHAPES: int = 10;

# SDF Material
var material_2d: ShaderMaterial;
# Shape types
var shape_types: Array[int];
# Shape transforms
var shape_positions: Array[Vector2];
var shape_scales: Array[float];
var shape_rotations: Array[float];
# Shape material
var shape_colors: Array[Color];
# Shape data
var shape_data0: Array[float];
var shape_data1: Array[float];
var shape_data2: Array[float];
var shape_data3: Array[float];
var shape_data4: Array[float];
var shape_data5: Array[float];
var shape_data6: Array[float];
var shape_data7: Array[float];


func _ready():
	shape_types.resize(MAX_SHAPES);
	shape_positions.resize(MAX_SHAPES);
	shape_scales.resize(MAX_SHAPES);
	shape_rotations.resize(MAX_SHAPES);
	shape_colors.resize(MAX_SHAPES);
	shape_data0.resize(MAX_SHAPES);
	shape_data1.resize(MAX_SHAPES);
	shape_data2.resize(MAX_SHAPES);
	shape_data3.resize(MAX_SHAPES);
	shape_data4.resize(MAX_SHAPES);
	shape_data5.resize(MAX_SHAPES);
	shape_data6.resize(MAX_SHAPES);
	shape_data7.resize(MAX_SHAPES);


func _process(_delta):
	reset_shader_params();
	
	for i in self.get_children().size():
		var child := self.get_child(i) as SdfShape2D;
		if child == null: continue;
		if not child.visible: continue;
		
		# Shape type
		shape_types[i] = child.shape_type;
		# Shape transform
		shape_positions[i] = child.global_position;
		shape_scales[i] = child.global_scale.x;
		shape_rotations[i] = child.global_rotation;
		# Shape material
		shape_colors[i] = child.shape_material.color;
		# Shape data
		shape_data0[i] = child.data0;
		shape_data1[i] = child.data1;
		shape_data2[i] = child.data2;
		shape_data3[i] = child.data3;
		shape_data4[i] = child.data4;
		shape_data5[i] = child.data5;
		shape_data6[i] = child.data6;
		shape_data7[i] = child.data7;
	
	set_shader_uniforms();


func set_shader_uniforms() -> void:
	material_2d.set_shader_parameter("shape_types", shape_types);
	material_2d.set_shader_parameter("shape_positions", shape_positions);
	material_2d.set_shader_parameter("shape_scales", shape_scales);
	material_2d.set_shader_parameter("shape_rotations", shape_rotations);
	material_2d.set_shader_parameter("shape_colors", shape_colors);
	material_2d.set_shader_parameter("shape_data0", shape_data0);
	material_2d.set_shader_parameter("shape_data1", shape_data1);
	material_2d.set_shader_parameter("shape_data2", shape_data2);
	material_2d.set_shader_parameter("shape_data3", shape_data3);
	material_2d.set_shader_parameter("shape_data4", shape_data4);
	material_2d.set_shader_parameter("shape_data5", shape_data5);
	material_2d.set_shader_parameter("shape_data6", shape_data6);
	material_2d.set_shader_parameter("shape_data7", shape_data7);


func reset_shader_params():
	for i in MAX_SHAPES:
		# Shape type
		shape_types[i] = 0;
		# Shape transform
		shape_positions[i] = Vector2(0, 0);
		shape_scales[i] = 0;
		shape_rotations[i] = 0;
		# Shape material
		shape_colors[i] = 0;
		# Shape data
		shape_data0[i] = 0;
		shape_data1[i] = 0;
		shape_data2[i] = 0;
		shape_data3[i] = 0;
		shape_data4[i] = 0;
		shape_data5[i] = 0;
		shape_data6[i] = 0;
		shape_data7[i] = 0;
