shader_type canvas_item;

#include "res://shaders/utils/utils.gdshaderinc"
#include "3d_types.gdshaderinc"
#include "3d_primitives.gdshaderinc"
#include "3d_operations.gdshaderinc"

#define MAX_SHAPES 10
#define FLOAT_MAX 1.0 / 0.0

#define MAX_RAYMARCH_ITERATIONS 400
#define MAX_SHADOW_ITERATIONS 100

uniform sampler2D skybox_texture: filter_linear, source_color;

uniform vec3 camera_position;
uniform vec3 camera_target;

uniform int shape_count;
uniform uint shape_types[MAX_SHAPES];

uniform vec3 shape_material_colors[MAX_SHAPES];
uniform sampler2D shape_material_textures[MAX_SHAPES]: filter_linear, source_color;

uniform vec3 shape_transform_positions[MAX_SHAPES];
uniform vec3 shape_transform_rotations[MAX_SHAPES];
uniform float shape_transform_scales[MAX_SHAPES];

uniform float shape_data0[MAX_SHAPES];
uniform float shape_data1[MAX_SHAPES];
uniform float shape_data2[MAX_SHAPES];
uniform float shape_data3[MAX_SHAPES];
uniform float shape_data4[MAX_SHAPES];
uniform float shape_data5[MAX_SHAPES];
uniform float shape_data6[MAX_SHAPES];
uniform float shape_data7[MAX_SHAPES];

float compute_sdf(vec3 position, uint shape_type, float data0, float data1, float data2, float data3, float data4, float data5, float data6, float data7) {
	if (shape_type == SHAPE_SPHERE) return sdf_sphere(position, data0);
	else if (shape_type == SHAPE_PLANE) return sdf_plane(position, 0.0);
	else return FLOAT_MAX;
}

// Ray Marching functions

vec3 transform_space(vec3 position, vec3 shape_position, float shape_scale, vec3 shape_rotation) {
	vec3 transformed_position = position - shape_position;
	return transformed_position;
}

SdfResult compute_scene_distance(vec3 position) {
	SdfResult result;
	for (int i = 0; i < shape_count; i++) {
		vec3 transformed_position = transform_space(
			position, 
			shape_transform_positions[i], 
			shape_transform_scales[i], 
			shape_transform_rotations[i]
		);
		float sdf = compute_sdf(transformed_position, shape_types[i], 
			shape_data0[i], shape_data1[i], shape_data2[i], shape_data3[i], 
			shape_data4[i], shape_data5[i], shape_data6[i], shape_data7[i]);
		
		if (i == 0) {
			result.sdf = sdf;
			result.shape_id = i;
			continue;
		}
		
		result = op_union(result.sdf, sdf, result.shape_id, i);
	}
	
	return result;
}

vec3 compute_normal(vec3 position) {
	// This just computes an approximation of the gradient of the sdf of the
	// scene, which is equivalent to the normal of the surface
	vec2 e = vec2(1e-4, 0.0);
	float dfdx = compute_scene_distance(position + e.xyy).sdf 
			   - compute_scene_distance(position - e.xyy).sdf;
	float dfdy = compute_scene_distance(position + e.yxy).sdf
			   - compute_scene_distance(position - e.yxy).sdf;
	float dfdz = compute_scene_distance(position + e.yyx).sdf
			   - compute_scene_distance(position - e.yyx).sdf;
	return normalize(vec3(dfdx, dfdy, dfdz));
}

float march_ray(inout Ray ray, Camera camera) {
	float traveled_distance = 0.0;
	ray.hit_info.closest_distance = FLOAT_MAX;
	for (int i = 0; i < MAX_RAYMARCH_ITERATIONS; i++) {
		ray.hit_info.iterations = i;
		ray.position = ray.origin + traveled_distance * ray.direction;
		
		// Calculate the distance to the scene and check if we hit something 
		SdfResult scene = compute_scene_distance(ray.position);
		if (scene.sdf < ray.hit_info.closest_distance) {
			ray.hit_info.closest_shape_id = scene.shape_id;
			ray.hit_info.closest_distance = scene.sdf;
		}
		
		// We take the absolute value of the scene distance here to allow for
		// the ray to backtrack and avoid over-stepping artifacts
		// We multiply the tolerance by the traveled distance to implement
		// cone tracing
		if (abs(scene.sdf) < ray.hit_info.tolerance * traveled_distance) { 
			// Ray hit something!
			ray.hit_info.hit = true;
			ray.hit_info.normal = compute_normal(ray.position);
			ray.hit_info.closest_distance = scene.sdf;
			ray.hit_info.closest_shape_id = scene.shape_id;
			break;
		}
		
		// Update the total traveled distance and check far plane
		traveled_distance += scene.sdf;
		if (traveled_distance > camera.far_plane) break; // Ray went too far...
	}
	
	return traveled_distance;
}

float cast_shadow(Ray ray, Camera camera, float sharpness) {
	float result = 1.0;
	
	float traveled_distance = 0.0;
	for (int i = 0; i < MAX_SHADOW_ITERATIONS; i++) {
		ray.position = ray.origin + traveled_distance * ray.direction;
		
		// Calculate the distance to the scene
		SdfResult scene = compute_scene_distance(ray.position);
		// Calculate soft shadow result
		result = min(result, sharpness * scene.sdf / traveled_distance);
		
		// Update the traveled distance and check if we hit the far plane
		traveled_distance += scene.sdf;
		if (traveled_distance > camera.far_plane) break;
	}
	
	return result;
}

float compute_ambient_occlusion(Ray ray) {
	float occlusion = 0.0;
	float occlusion_scale = 1.0;
	// Multi-sampling based ambient occlusion
	for (int i = 0; i < 5; i++) {
		float sample_offset = 0.01 + 0.11 * float(i) / 4.0;
		vec3 sample_position = ray.position + sample_offset * ray.hit_info.normal;
		float occlusion_distance = compute_scene_distance(sample_position).sdf;
		occlusion += (sample_offset - occlusion_distance) * occlusion_scale;
		occlusion_scale *= 0.95;
	}
	return saturate(1.0 - 2.0 * occlusion);
}

vec3 compute_lighting(Ray ray, Camera camera, Sun sun, Sky sky) {
	vec3 color = sky.color - 0.7 * ray.direction.y;
	color = mix(color, sky.horizon_color, exp(-10.0 * ray.direction.y));
	
	vec3 normal = normalize(ray.direction);
	vec2 skybox_uv = vec2(atan(normal.x, normal.z) / (2.0 * PI), normal.y / (PI * 0.7)) + 0.5;
	vec4 skybox_color = texture(skybox_texture, skybox_uv);
	color = skybox_color.xyz;
	
	if (ray.hit_info.hit) {
		// Handling the Sun
		vec3 sun_direction = normalize(shape_transform_positions[0] - ray.position);
		if (ray.hit_info.closest_shape_id == 0) sun_direction = -(shape_transform_positions[0] - ray.position);
		float sun_diffuse = saturate(dot(ray.hit_info.normal, sun_direction));
		Ray sun_shadow_ray = create_ray(
			ray.position + ray.hit_info.normal * 0.001, sun_direction
		);
		float sun_shadow = ray.hit_info.closest_shape_id != 0 ? cast_shadow(sun_shadow_ray, camera, 15.0) : 1.0;
		
		// Handling the Sky
		float sky_diffuse 
			= saturate(0.5 + 0.5 * dot(ray.hit_info.normal, vec3(0.0, 1.0, 0.0)));
		
		// Ambient occlusion
		float occlusion = compute_ambient_occlusion(ray);
		
		// Base material
		vec3 position = ray.position - shape_transform_positions[ray.hit_info.closest_shape_id];
		float shape_radius = shape_data0[ray.hit_info.closest_shape_id];
		vec2 shape_uv = vec2(atan(position.x, position.z) / (2.0 * PI), -position.y / (2.0 * shape_radius)) + 0.5;
		vec3 shape_color = texture(shape_material_textures[ray.hit_info.closest_shape_id], shape_uv).xyz;
		vec3 material = shape_color * 0.18;
		
		// Calculating final color
		color = sun.color * sun_diffuse * sun_shadow; // Sun contribution
		color += sun_diffuse * sun_diffuse * vec3(10.0);
		color += 2.0 * sky.color * sky_diffuse * occlusion; // Sky contribution
		color *= material; // Object material
		
		// Color curve
		color = pow(color, vec3(0.7, 0.9, 1.0));
	}
	
	return color;
}

// Main function

#define AA 2
void fragment() {
	// Initialize scene objects
	Sun sun = create_sun(normalize(vec3(1.0, 0.8, 0.6)), vec3(7.0, 4.5, 3.0));
	Sky sky = create_sky(vec3(0.4, 0.75, 1.0), vec3(0.7, 0.75, 0.8));
//	Camera camera = create_camera(vec3(1.0 * sin(TIME * 0.5) + 1.8, 0.3, 1.0 * cos(TIME * 0.5)), vec3(1.8, 0.0, 0.0), 1.5, 20.0, 1.0);
	Camera camera = create_camera(camera_position, camera_target, 1.5, 20.0, 1.0);
	
	vec3 color = vec3(0.0);
	for (int i = 0; i < AA; i++) {
		for (int j = 0; j < AA; j++) {
			vec2 offset = vec2(float(i), float(j)) / float(AA) * 2.6;
			
			// Normalize UV coordinates to the center of the viewport
			vec2 uv = (2.0 * (FRAGCOORD.xy + offset) - 1.0 / SCREEN_PIXEL_SIZE) * SCREEN_PIXEL_SIZE.y;
			uv.y *= -1.0; // To make y point upward instead of downward
			
			// Initialize one ray per antialiased sample
			Ray ray = create_ray_from_camera(camera, uv);
			
			// Raymarch
			march_ray(ray, camera);
			
			// Calcualte the color of the scene
			color += compute_lighting(ray, camera, sun, sky);
		}
	}
	color /= float(AA*AA);
	
	// Gamma correction
//	color = pow(color, vec3(0.4545));
	
	// Color grading
	color = clamp(camera.gain * color, 0.0, 1.0);
	vec3 color_corrected = color * color * (3.0 - 2.0 * color);
	color = mix(color, color_corrected, 0.4);
	
	COLOR = vec4(color, 1.0);
}
