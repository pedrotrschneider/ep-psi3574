/*
 * Almost all of these functions are based on Inigo Quilez's formulas:
 * https://iquilezles.org/articles/distfunctions2d/
 * All credits to him.
 */

#define PI 3.14159265359
#define MAX_VERTICES 4

/*************************************************
 *               SHAPE DEFINITIONS               *
 *************************************************/

const uint SHAPE_NONE = uint(0);
const uint SHAPE_CIRCLE = uint(1);
const uint SHAPE_BOX = uint(2);
const uint SHAPE_ROUNDED_BOX = uint(3);
const uint SHAPE_ORIENTED_BOX = uint(4);
const uint SHAPE_SEGMENT = uint(5);
const uint SHAPE_THICK_SEGMENT = uint(6);
const uint SHAPE_RHOMBUS = uint(7);
const uint SHAPE_TRAPEZOID = uint(8);
const uint SHAPE_PARALLELOGRAM = uint(9);
const uint SHAPE_EQUILATERAL_TRIANGLE = uint(10);
const uint SHAPE_ISOCELES_TRIANGLE = uint(11);
const uint SHAPE_TRIANGLE = uint(12);
const uint SHAPE_UNEVEN_CAPSULE = uint(13);
const uint SHAPE_PENTAGON = uint(14);
const uint SHAPE_HEXAGON = uint(15);
const uint SHAPE_NGON = uint(16);
const uint SHAPE_STAR = uint(17);
const uint SHAPE_PIE = uint(18);
const uint SHAPE_ARC = uint(19);
const uint SHAPE_HORSESHOE = uint(20);
const uint SHAPE_VESICA = uint(21);
const uint SHAPE_MOON = uint(22);
const uint SHAPE_ROUNDED_CROSS = uint(23);
const uint SHAPE_EGG = uint(24);
const uint SHAPE_HEART = uint(25);
const uint SHAPE_CROSS = uint(26);
const uint SHAPE_POLYGON = uint(27);
const uint SHAPE_ELLIPSE = uint(28);
const uint SHAPE_PARABOLA = uint(29);
const uint SHAPE_PARABOLA_SEGMENT = uint(30);
const uint SHAPE_BEZIER = uint(31);
const uint SHAPE_BLOBBY_CROSS = uint(32);
const uint SHAPE_TUNNEL = uint(33);
const uint SHAPE_STAIRS = uint(34);
const uint SHAPE_QUADRATIC_CIRCLE = uint(35);
const uint SHAPE_HYPERBOLA = uint(36);
const uint SHAPE_COOL_S = uint(37);
const uint SHAPE_CIRCLE_WAVE = uint(38);
const uint SHAPE_ARROW = uint(39);
const uint SHAPE_CIRCLE_JOINT = uint(40);
const uint SHAPE_FLAT_JOINT = uint(41);
const uint SHAPE_HYPERBOLIC_CROSS = uint(42);
const uint SHAPE_SPIRAL = uint(43);

/*************************************************
 *                     UTILS                     *
 *************************************************/

float ndot(vec2 a, vec2 b) { return a.x * b.x - a.y * b.y; }

float dot2(vec2 a) { return dot(a, a); }

/*************************************************
 *               PRIMITIVE SHAPES                *
 *************************************************/

float sdf_circle(vec2 uv, float radius) { return length(uv) - radius; }

float sdf_box(vec2 uv, vec2 bounds) {
    vec2 d = abs(uv) - bounds;
    return length(max(d, 0.0)) + min(max(d.x, d.y), 0.0);
}

float sdf_rounded_box(vec2 uv, vec2 bounds, vec4 radius) {
    radius.xy = (uv.x > 0.0) ? radius.xy : radius.zw;
    radius.x = (uv.y > 0.0) ? radius.x : radius.y;
    vec2 q = abs(uv) - bounds + radius.x;
    return min(max(q.x, q.y), 0.0) + length(max(q, 0.0)) - radius.x;
}

float sdf_oriented_box(vec2 uv, vec2 vertex1, vec2 vertex2, float thickness) {
    float l = length(vertex2 - vertex1);
    vec2 d = (vertex2 - vertex1) / l;
    vec2 q = (uv - (vertex1 + vertex2) * 0.5);
    q = mat2(vec2(d.x, -d.y), vec2(d.y, d.x)) * q;
    q = abs(q) - vec2(l, thickness) * 0.5;
    return length(max(q, 0.0)) + min(max(q.x, q.y), 0.0);
}

float sdf_segment(vec2 uv, vec2 vertex1, vec2 vertex2) {
    vec2 pa = uv - vertex1, ba = vertex2 - vertex1;
    float h = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
    return length(pa - ba * h);
}

float sdf_thick_segment(vec2 uv, vec2 vertex1, vec2 vertex2, float radius) {
    return sdf_segment(uv, vertex1, vertex2) - radius;
}

float sdf_rhombus(vec2 uv, vec2 bounds) {
    uv = abs(uv);
    float h =
        clamp(ndot(bounds - 2.0 * uv, bounds) / dot(bounds, bounds), -1.0, 1.0);
    float d = length(uv - 0.5 * bounds * vec2(1.0 - h, 1.0 + h));
    return d * sign(uv.x * bounds.y + uv.y * bounds.x - bounds.x * bounds.y);
}

float sdf_trapezoid(vec2 uv, float bottom_base, float top_base, float height) {
    vec2 k1 = vec2(top_base, height);
    vec2 k2 = vec2(top_base - bottom_base, 2.0 * height);
    uv.x = abs(uv.x);
    vec2 ca = vec2(uv.x - min(uv.x, (uv.y < 0.0) ? bottom_base : top_base),
                   abs(uv.y) - height);
    vec2 cb = uv - k1 + k2 * clamp(dot(k1 - uv, k2) / dot2(k2), 0.0, 1.0);
    float s = (cb.x < 0.0 && ca.y < 0.0) ? -1.0 : 1.0;
    return s * sqrt(min(dot2(ca), dot2(cb)));
}

float sdf_parallelogram(vec2 uv, float width, float height, float skew) {
    vec2 e = vec2(skew, height);
    uv = (uv.y < 0.0) ? -uv : uv;
    vec2 w = uv - e;
    w.x -= clamp(w.x, -width, width);
    vec2 d = vec2(dot(w, w), -w.y);
    float s = uv.x * e.y - uv.y * e.x;
    uv = (s < 0.0) ? -uv : uv;
    vec2 v = uv - vec2(width, 0);
    v -= e * clamp(dot(v, e) / dot(e, e), -1.0, 1.0);
    d = min(d, vec2(dot(v, v), width * height - abs(s)));
    return sqrt(d.x) * sign(-d.y);
}

float sdf_equilateral_triangle(vec2 uv, float side) {
    const float k = sqrt(3.0);
    uv.x = abs(uv.x) - side;
    uv.y = uv.y + side / k;
    if (uv.x + k * uv.y > 0.0)
        uv = vec2(uv.x - k * uv.y, -k * uv.x - uv.y) / 2.0;
    uv.x -= clamp(uv.x, -2.0 * side, 0.0);
    return -length(uv) * sign(uv.y);
}

float sdf_isoceles_triangle(vec2 top, vec2 base_center) {
    top.x = abs(top.x);
    vec2 a = top - base_center * clamp(dot(top, base_center) /
                                           dot(base_center, base_center),
                                       0.0, 1.0);
    vec2 b =
        top - base_center * vec2(clamp(top.x / base_center.x, 0.0, 1.0), 1.0);
    float s = -sign(base_center.y);
    vec2 d = min(
        vec2(dot(a, a), s * (top.x * base_center.y - top.y * base_center.x)),
        vec2(dot(b, b), s * (top.y - base_center.y)));
    return -sqrt(d.x) * sign(d.y);
}

float sdf_triangle(vec2 uv, vec2 vertex1, vec2 vertex2, vec2 vertex3) {
    vec2 e0 = vertex2 - vertex1, e1 = vertex3 - vertex2, e2 = vertex1 - vertex3;
    vec2 v0 = uv - vertex1, v1 = uv - vertex2, v2 = uv - vertex3;
    vec2 pq0 = v0 - e0 * clamp(dot(v0, e0) / dot(e0, e0), 0.0, 1.0);
    vec2 pq1 = v1 - e1 * clamp(dot(v1, e1) / dot(e1, e1), 0.0, 1.0);
    vec2 pq2 = v2 - e2 * clamp(dot(v2, e2) / dot(e2, e2), 0.0, 1.0);
    float s = sign(e0.x * e2.y - e0.y * e2.x);
    vec2 d = min(min(vec2(dot(pq0, pq0), s * (v0.x * e0.y - v0.y * e0.x)),
                     vec2(dot(pq1, pq1), s * (v1.x * e1.y - v1.y * e1.x))),
                 vec2(dot(pq2, pq2), s * (v2.x * e2.y - v2.y * e2.x)));
    return -sqrt(d.x) * sign(d.y);
}

float sdf_uneven_capsule(vec2 uv, float radius_bottom, float radius_top,
                         float height) {
    uv.x = abs(uv.x);
    float b = (radius_bottom - radius_top) / height;
    float a = sqrt(1.0 - b * b);
    float k = dot(uv, vec2(-b, a));
    if (k < 0.0)
        return length(uv) - radius_bottom;
    if (k > a * height)
        return length(uv - vec2(0.0, height)) - radius_top;
    return dot(uv, vec2(a, b)) - radius_bottom;
}

float sdf_pentagon(vec2 uv, float internal_radius) {
    const vec3 k = vec3(0.809016994, 0.587785252, 0.726542528);
    uv.x = abs(uv.x);
    uv -= 2.0 * min(dot(vec2(-k.x, k.y), uv), 0.0) * vec2(-k.x, k.y);
    uv -= 2.0 * min(dot(vec2(k.x, k.y), uv), 0.0) * vec2(k.x, k.y);
    uv -= vec2(clamp(uv.x, -internal_radius * k.z, internal_radius * k.z),
               internal_radius);
    return length(uv) * sign(uv.y);
}

float sdf_hexagon(vec2 uv, float internal_radius) {
    const vec3 k = vec3(-0.866025404, 0.5, 0.577350269);
    uv = abs(uv);
    uv -= 2.0 * min(dot(k.xy, uv), 0.0) * k.xy;
    uv -= vec2(clamp(uv.x, -k.z * internal_radius, k.z * internal_radius),
               internal_radius);
    return length(uv) * sign(uv.y);
}

float sdf_ngon(vec2 uv, float radius, int points) {
    float an = PI / float(points);
    vec2 acs = vec2(cos(an), sin(an));
    vec2 ecs = vec2(0, 1);

    float bn = mod(atan(uv.x, uv.y), 2.0 * an) - an;
    uv = length(uv) * vec2(cos(bn), abs(sin(bn)));
    uv -= radius * acs;
    uv += ecs * clamp(-dot(uv, ecs), 0.0, radius * acs.y / ecs.y);
    return length(uv) * sign(uv.x);
}

float sdf_star(vec2 uv, float radius, int points, float inflate) {
    inflate = (1.0 - inflate) * float(points - 2) + 2.0;

    float an = PI / float(points);
    float en = PI / inflate;
    vec2 acs = vec2(cos(an), sin(an));
    vec2 ecs = vec2(cos(en), sin(en));

    float bn = mod(atan(uv.x, uv.y), 2.0 * an) - an;
    uv = length(uv) * vec2(cos(bn), abs(sin(bn)));
    uv -= radius * acs;
    uv += ecs * clamp(-dot(uv, ecs), 0.0, radius * acs.y / ecs.y);
    return length(uv) * sign(uv.x);
}

float sdf_pie(vec2 uv, float angle, float radius) {
    angle *= 0.5;
    vec2 c = vec2(sin(angle), cos(angle));
    uv.x = abs(uv.x);
    float l = length(uv) - radius;
    float m = length(uv - c * clamp(dot(uv, c), 0.0, radius));
    return max(l, m * sign(c.y * uv.x - c.x * uv.y));
}

float sdf_arc(vec2 uv, float angle, float radius, float thickness) {
    angle *= 0.5;
    vec2 sc = vec2(sin(angle), cos(angle));
    // sc is the sin/cos of the arc's aperture
    uv.x = abs(uv.x);
    return ((sc.y * uv.x > sc.x * uv.y) ? length(uv - sc * radius)
                                        : abs(length(uv) - radius)) -
           thickness;
}

float sdf_horseshoe(vec2 uv, float angle, float radius, float elongation,
                    float thickness) {
    angle *= 0.5;
    vec2 c = vec2(cos(angle), sin(angle));
    uv.x = abs(uv.x);
    float l = length(uv);
    uv = mat2(vec2(-c.x, c.y), vec2(c.y, c.x)) * uv;
    uv = vec2((uv.y > 0.0 || uv.x > 0.0) ? uv.x : l * sign(-c.x),
              (uv.x > 0.0) ? uv.y : l);
    uv = vec2(uv.x - elongation, abs(uv.y - radius) - thickness);
    return length(max(uv, 0.0)) + min(0.0, max(uv.x, uv.y));
}

float sdf_vesica(vec2 uv, float radius, float width) {
    width = 1.0 - width;
    uv = abs(uv);
    float b = sqrt(radius * radius - width * width);
    return ((uv.y - b) * width > uv.x * b)
               ? length(uv - vec2(0.0, b))
               : length(uv - vec2(-width, 0.0)) - radius;
}

float sdf_oriented_vesica(vec2 uv, vec2 vertex1, vec2 vertex2, float width) {
    float r = 0.5 * length(vertex2 - vertex1);
    float d = 0.5 * (r * r - width * width) / width;
    vec2 v = (vertex2 - vertex1) / r;
    vec2 c = (vertex2 + vertex1) * 0.5;
    vec2 q = 0.5 * abs(mat2(vec2(v.y, v.x), vec2(-v.x, v.y)) * (uv - c));
    vec3 h = (r * q.x < d * (q.y - r)) ? vec3(0.0, r, 0.0)
                                       : vec3(-d, 0.0, d + width);
    return length(q - h.xy) - h.z;
}

float sdf_moon(vec2 uv, float offset, float moon_radius, float shadow_radius) {
    uv.y = abs(uv.y);
    float a = (moon_radius * moon_radius - shadow_radius * shadow_radius +
               offset * offset) /
              (2.0 * offset);
    float b = sqrt(max(moon_radius * moon_radius - a * a, 0.0));
    if (offset * (uv.x * b - uv.y * a) > offset * offset * max(b - uv.y, 0.0))
        return length(uv - vec2(a, b));
    return max((length(uv) - moon_radius),
               -(length(uv - vec2(offset, 0)) - shadow_radius));
}

float sdf_rounded_cross(vec2 uv, float height) {
    float k = 0.5 * (height + 1.0 / height);
    uv = abs(uv);
    return (uv.x < 1.0 && uv.y < uv.x * (k - height) + height)
               ? k - sqrt(dot2(uv - vec2(1, k)))
               : sqrt(min(dot2(uv - vec2(0, height)), dot2(uv - vec2(1, 0))));
}

float sdf_egg(vec2 uv, float radius, float elongation) {
	const float k = sqrt(3.0);
	uv.x = abs(uv.x);
	float r = radius - elongation;
	return ((uv.y < 0.0) 
				? length(vec2(uv.x, uv.y)) - r
				: ((k * (uv.x + r) < uv.y)
					? length(vec2(uv.x, uv.y - k * r))
					: length(vec2(uv.x + r, uv.y)) - 2.0 * r) - elongation);
}

float sdf_heart(vec2 uv) {
    uv.x = abs(uv.x);

    if (uv.y + uv.x > 1.0)
        return sqrt(dot2(uv - vec2(0.25, 0.75))) - sqrt(2.0) / 4.0;
    return sqrt(min(dot2(uv - vec2(0.00, 1.00)),
                    dot2(uv - 0.5 * max(uv.x + uv.y, 0.0)))) *
           sign(uv.x - uv.y);
}

float sdf_cross(vec2 uv, float internal_length, float external_length,
                float roundness) {
    vec2 b = vec2(internal_length, external_length);
    uv = abs(uv);
    uv = (uv.y > uv.x) ? uv.yx : uv.xy;
    vec2 q = uv - b;
    float k = max(q.y, q.x);
    vec2 w = (k > 0.0) ? q : vec2(b.y - uv.x, -k);
    return sign(k) * length(max(w, 0.0)) + roundness;
}

float sdf_rounded_x(vec2 uv, float width, float radius) {
    uv = abs(uv);
    return length(uv - min(uv.x + uv.y, width) * 0.5) - radius;
}

float sdf_polygon(vec2 uv, vec2[MAX_VERTICES] vertices,
                  const int vertex_count) {
    float d = dot(uv - vertices[0], uv - vertices[0]);
    float s = 1.0;
    for (int i = 0, j = vertex_count - 1; i < vertex_count; j = i, i++) {
        // distance
        vec2 e = vertices[j] - vertices[i];
        vec2 w = uv - vertices[i];
        vec2 b = w - e * clamp(dot(w, e) / dot(e, e), 0.0, 1.0);
        d = min(d, dot(b, b));

        // winding number from http://geomalgorithms.com/a03-_inclusion.html
        bvec3 cond = bvec3(uv.y >= vertices[i].y,
                           uv.y<vertices[j].y, e.x * w.y> e.y * w.x);
        if (all(cond) || all(not(cond)))
            s = -s;
    }

    return s * sqrt(d);
}

float sdf_ellipse(vec2 uv, vec2 bounds) {
    uv = abs(uv);
    if (uv.x > uv.y) {
        uv = uv.yx;
        bounds = bounds.yx;
    }
    float l = bounds.y * bounds.y - bounds.x * bounds.x;
    float m = bounds.x * uv.x / l;
    float m2 = m * m;
    float n = bounds.y * uv.y / l;
    float n2 = n * n;
    float c = (m2 + n2 - 1.0) / 3.0;
    float c3 = c * c * c;
    float q = c3 + m2 * n2 * 2.0;
    float d = c3 + m2 * n2;
    float g = m + m * n2;
    float co;
    if (d < 0.0) {
        float h = acos(q / c3) / 3.0;
        float s = cos(h);
        float t = sin(h) * sqrt(3.0);
        float rx = sqrt(-c * (s + t + 2.0) + m2);
        float ry = sqrt(-c * (s - t + 2.0) + m2);
        co = (ry + sign(l) * rx + abs(g) / (rx * ry) - m) / 2.0;
    } else {
        float h = 2.0 * m * n * sqrt(d);
        float s = sign(q + h) * pow(abs(q + h), 1.0 / 3.0);
        float u = sign(q - h) * pow(abs(q - h), 1.0 / 3.0);
        float rx = -s - u - c * 4.0 + 2.0 * m2;
        float ry = (s - u) * sqrt(3.0);
        float rm = sqrt(rx * rx + ry * ry);
        co = (ry / sqrt(rm - rx) + 2.0 * g / rm - m) / 2.0;
    }
    vec2 r = bounds * vec2(co, sqrt(1.0 - co * co));
    return length(r - uv) * sign(uv.y - r.y);
}

float sdf_parabola(vec2 uv, float k) {
    uv.x = abs(uv.x);
    float ik = 1.0 / k;
    float p = ik * (uv.y - 0.5 * ik) / 3.0;
    float q = 0.25 * ik * ik * uv.x;
    float h = q * q - p * p * p;
    float r = sqrt(abs(h));
    float x = (h > 0.0) ? pow(q + r, 1.0 / 3.0) -
                              pow(abs(q - r), 1.0 / 3.0) * sign(r - q)
                        : 2.0 * cos(atan(r, q) / 3.0) * sqrt(p);
    return length(uv - vec2(x, k * x * x)) * sign(uv.x - x);
}

float sdf_parabola_segment(vec2 uv, float width, float height) {
    uv.x = abs(uv.x);
    float ik = width * width / height;
    float p = ik * (height - uv.y - 0.5 * ik) / 3.0;
    float q = uv.x * ik * ik * 0.25;
    float h = q * q - p * p * p;
    float r = sqrt(abs(h));
    float x = (h > 0.0) ? pow(q + r, 1.0 / 3.0) -
                              pow(abs(q - r), 1.0 / 3.0) * sign(r - q)
                        : 2.0 * cos(atan(r / q) / 3.0) * sqrt(p);
    x = min(x, width);
    return length(uv - vec2(x, height - x * x / ik)) *
           sign(ik * (uv.y - height) + uv.x * uv.x);
}

float sdf_bezier(vec2 uv, vec2 point1, vec2 point2, vec2 handle) {
    vec2 a = handle - point1;
    vec2 b = point1 - 2.0 * handle + point2;
    vec2 c = a * 2.0;
    vec2 d = point1 - uv;
    float kk = 1.0 / dot(b, b);
    float kx = kk * dot(a, b);
    float ky = kk * (2.0 * dot(a, a) + dot(d, b)) / 3.0;
    float kz = kk * dot(d, a);
    float res = 0.0;
    float p = ky - kx * kx;
    float p3 = p * p * p;
    float q = kx * (2.0 * kx * kx - 3.0 * ky) + kz;
    float h = q * q + 4.0 * p3;
    if (h >= 0.0) {
        h = sqrt(h);
        vec2 x = (vec2(h, -h) - q) / 2.0;
        vec2 uv = sign(x) * pow(abs(x), vec2(1.0 / 3.0));
        float t = clamp(uv.x + uv.y - kx, 0.0, 1.0);
        res = dot2(d + (c + b * t) * t);
    } else {
        float z = sqrt(-p);
        float v = acos(q / (p * z * 2.0)) / 3.0;
        float m = cos(v);
        float n = sin(v) * 1.732050808;
        vec3 t = clamp(vec3(m + m, -n - m, n - m) * z - kx, 0.0, 1.0);
        res = min(dot2(d + (c + b * t.x) * t.x), dot2(d + (c + b * t.y) * t.y));
        // the third root cannot be the closest
        // res = min(res,dot2(d+(c+b*t.z)*t.z));
    }
    return sqrt(res);
}

float sdf_blobby_cross(vec2 uv, float thinness, float blobness) {
    uv = abs(uv);
    uv = vec2(abs(uv.x - uv.y), 1.0 - uv.x - uv.y) / sqrt(2.0);

    float p = (thinness - uv.y - 0.25 / thinness) / (6.0 * thinness);
    float q = uv.x / (thinness * thinness * 16.0);
    float h = q * q - p * p * p;

    float x;
    if (h > 0.0) {
        float r = sqrt(h);
        x = pow(q + r, 1.0 / 3.0) - pow(abs(q - r), 1.0 / 3.0) * sign(r - q);
    } else {
        float r = sqrt(p);
        x = 2.0 * r * cos(acos(q / (p * r)) / 3.0);
    }
    x = min(x, sqrt(2.0) / 2.0);

    vec2 z = vec2(x, thinness * (1.0 - 2.0 * x * x)) - uv;
    return length(z) * sign(z.y) - blobness;
}

float sdf_tunnel(vec2 uv, float radius, float height) {
    vec2 bounds = vec2(radius, height);
    uv.x = abs(uv.x);
    uv.y = -uv.y;
    vec2 q = uv - bounds;

    float d1 = dot2(vec2(max(q.x, 0.0), q.y));
    q.x = (uv.y > 0.0) ? q.x : length(uv) - bounds.x;
    float d2 = dot2(vec2(q.x, max(q.y, 0.0)));
    float d = sqrt(min(d1, d2));

    return (max(q.x, q.y) < 0.0) ? -d : d;
}

float sdf_stairs(vec2 uv, vec2 step_bounds, float step_count) {
    vec2 ba = step_bounds * step_count;
    float d = min(dot2(uv - vec2(clamp(uv.x, 0.0, ba.x), 0.0)),
                  dot2(uv - vec2(ba.x, clamp(uv.y, 0.0, ba.y))));
    float s = sign(max(-uv.y, uv.x - ba.x));

    float dia = length(step_bounds);
    uv = mat2(vec2(step_bounds.x, -step_bounds.y), vec2(step_bounds.y, step_bounds.x)) *
         uv / dia;
    float id = clamp(round(uv.x / dia), 0.0, step_count - 1.0);
    uv.x = uv.x - id * dia;
    uv = mat2(vec2(step_bounds.x, step_bounds.y), vec2(-step_bounds.y, step_bounds.x)) *
         uv / dia;

    float hh = step_bounds.y / 2.0;
    uv.y -= hh;
    if (uv.y > hh * sign(uv.x))
        s = 1.0;
    uv = (id < 0.5 || uv.x > 0.0) ? uv : -uv;
    d = min(d, dot2(uv - vec2(0.0, clamp(uv.y, -hh, hh))));
    d = min(d, dot2(uv - vec2(clamp(uv.x, 0.0, step_bounds.x), hh)));

    return sqrt(d) * s;
}

float sdf_quadratic_circle(vec2 uv) {
    uv = abs(uv);
    if (uv.y > uv.x)
        uv = uv.yx;

    float a = uv.x - uv.y;
    float b = uv.x + uv.y;
    float c = (2.0 * b - 1.0) / 3.0;
    float h = a * a + c * c * c;
    float t;
    if (h >= 0.0) {
        h = sqrt(h);
        t = sign(h - a) * pow(abs(h - a), 1.0 / 3.0) - pow(h + a, 1.0 / 3.0);
    } else {
        float z = sqrt(-c);
        float v = acos(a / (c * z)) / 3.0;
        t = -z * (cos(v) + sin(v) * 1.732050808);
    }
    t *= 0.5;
    vec2 w = vec2(-t, t) + 0.75 - t * t - uv;
    return length(w) * sign(a * a * 0.5 + b - 1.5);
}

float sdf_hyperbola(vec2 uv, float k, float size) {
    uv = abs(uv);
    uv = vec2(uv.x - uv.y, uv.x + uv.y) / sqrt(2.0);

    float x2 = uv.x * uv.x / 16.0;
    float y2 = uv.y * uv.y / 16.0;
    float r = k * (4.0 * k - uv.x * uv.y) / 12.0;
    float q = (x2 - y2) * k * k;
    float h = q * q + r * r * r;
    float u;
    if (h < 0.0) {
        float m = sqrt(-r);
        u = m * cos(acos(q / (r * m)) / 3.0);
    } else {
        float m = pow(sqrt(h) - q, 1.0 / 3.0);
        u = (m - r / m) / 2.0;
    }
    float w = sqrt(u + x2);
    float b = k * uv.y - x2 * uv.x * 2.0;
    float t = uv.x / 4.0 - w + sqrt(2.0 * x2 - u + b / w / 4.0);
    t = max(t, sqrt(size * size * 0.5 + k) - size / sqrt(2.0));
    float d = length(uv - vec2(t, k / t));
    return uv.x * uv.y < k ? d : -d;
}

float sdf_cool_s(vec2 uv) {
    float six = (uv.y < 0.0) ? -uv.x : uv.x;
    uv.x = abs(uv.x);
    uv.y = abs(uv.y) - 0.2;
    float rex = uv.x - min(round(uv.x / 0.4), 0.4);
    float aby = abs(uv.y - 0.2) - 0.6;

    float d = dot2(vec2(six, -uv.y) - clamp(0.5 * (six - uv.y), 0.0, 0.2));
    d = min(d, dot2(vec2(uv.x, -aby) - clamp(0.5 * (uv.x - aby), 0.0, 0.4)));
    d = min(d, dot2(vec2(rex, uv.y - clamp(uv.y, 0.0, 0.4))));

    float s = 2.0 * uv.x + aby + abs(aby + 0.4) - 0.4;
    return sqrt(d) * sign(s);
}

float sdf_circle_wave(vec2 uv, float radius, float height) {
    height = 3.1415927 * 5.0 / 6.0 * max(height, 0.0001);
    vec2 co = radius * vec2(sin(height), cos(height));
    uv.x = abs(mod(uv.x, co.x * 4.0) - co.x * 2.0);
    vec2 p1 = uv;
    vec2 p2 = vec2(abs(uv.x - 2.0 * co.x), -uv.y + 2.0 * co.y);
    float d1 = ((co.y * p1.x > co.x * p1.y) ? length(p1 - co)
                                            : abs(length(p1) - radius));
    float d2 = ((co.y * p2.x > co.x * p2.y) ? length(p2 - co)
                                            : abs(length(p2) - radius));
    return min(d1, d2);
}

float sdf_arrow(vec2 uv, vec2 start, vec2 end, float body_thickness,
                float head_thickness) {
    const float k = 3.0;
    vec2 ba = end - start;
    float l2 = dot(ba, ba);
    float l = sqrt(l2);

    uv = uv - start;
    uv = mat2(vec2(ba.x, -ba.y), vec2(ba.y, ba.x)) * uv / l;
    uv.y = abs(uv.y);
    vec2 pz = uv - vec2(l - head_thickness * k, head_thickness);

    vec2 q = uv;
    q.x -= clamp(q.x, 0.0, l - head_thickness * k);
    q.y -= body_thickness;
    float di = dot(q, q);
    q = pz;
    q.y -= clamp(q.y, body_thickness - head_thickness, 0.0);
    di = min(di, dot(q, q));
    if (uv.x < body_thickness) // conditional is optional
    {
        q = uv;
        q.y -= clamp(q.y, 0.0, body_thickness);
        di = min(di, dot(q, q));
    }
    if (pz.x > 0.0) // conditional is optional
    {
        q = pz;
        q -= vec2(k, -1.0) *
             clamp((q.x * k - q.y) / (k * k + 1.0), 0.0, head_thickness);
        di = min(di, dot(q, q));
    }

    float si = 1.0;
    float z = l - uv.x;
    if (min(uv.x, z) > 0.0) {
        float h = (pz.x < 0.0) ? body_thickness : z / k;
        if (uv.y < h)
            si = -1.0;
    }
    return si * sqrt(di);
}

vec3 sdf_circle_joint(vec2 uv, float len, float angle, float width) {
    // if perfectly straight
    if (abs(angle) < 0.001) {
        float v = uv.y;
        uv.y -= clamp(uv.y, 0.0, len);
        return vec3(length(uv), uv.x, v);
    }

    // parameters
    vec2 sc = vec2(sin(angle), cos(angle));
    float ra = 0.5 * len / angle;

    // recenter
    uv.x -= ra;

    // reflect
    vec2 q = uv - 2.0 * sc * max(0.0, dot(sc, uv));

    // distance
    float u = abs(ra) - length(q);
    float d = (q.y < 0.0) ? length(q + vec2(ra, 0.0)) : abs(u);

    // parametrization (optional)
    float s = sign(angle);
    float v = ra * atan(s * uv.y, -s * uv.x);
    // if( v<0.0 ) v+=sign(a)*6.283185*ra;
    u = u * s;
    if (v < 0.0) {
        if (s * uv.x > 0.0) {
            v = abs(ra) * 6.283185 + v;
        } else {
            v = uv.y;
            u = q.x + ra;
        }
    }

    return vec3(d - width, u, v);
}

vec3 sdf_flat_joint(vec2 uv, float len, float angle, float width) {
    // if perfectly straight
    if (abs(angle) < 0.001) {
        float v = uv.y;
        uv.y -= clamp(uv.y, 0.0, len);
        return vec3(length(uv), uv.x, v);
    }

    // parameters
    vec2 sc = vec2(sin(angle), cos(angle));
    float ra = 0.5 * len / angle;

    // recenter
    uv.x -= ra;

    // reflect
    vec2 q = uv - 2.0 * sc * max(0.0, dot(sc, uv));

    // distance
    float u = abs(ra) - length(q);
    float d = max(length(vec2(q.x + ra - clamp(q.x + ra, -width, width), q.y)) *
                      sign(-q.y),
                  abs(u) - width);

    // parametrization (optional)
    float s = sign(angle);
    float v = ra * atan(s * uv.y, -s * uv.x);
    if (v < 0.0)
        v += sign(angle) * 6.283185 * ra;
    u = u * s;

    return vec3(d, u, v);
}

float sdf_hyperbolic_cross(vec2 uv, float k) {
    float s = 1.0 / k - k;
    uv = uv * s;
    uv = abs(uv);
    uv = (uv.x > uv.y) ? uv.yx : uv.xy;
    uv += k;

    float x2 = uv.x * uv.x / 16.0;
    float y2 = uv.y * uv.y / 16.0;
    float r = (uv.x * uv.y - 4.0) / 12.0;
    float q = y2 - x2;
    float h = q * q - r * r * r;
    float u;
    if (h < 0.0) {
        float m = sqrt(r);
        u = m * cos(acos(q / (r * m)) / 3.0);
    } else {
        float m = pow(sqrt(h) + q, 1.0 / 3.0);
        u = (m + r / m) / 2.0;
    }
    float w = sqrt(u + x2);
    float x = uv.x / 4.0 - w +
              sqrt(2.0 * x2 - u + (uv.y - x2 * uv.x * 2.0) / w / 4.0);

    x = max(x, k);

    float d = length(uv - vec2(x, 1.0 / x)) / s;

    return uv.x * uv.y < 1.0 ? -d : d;
}

float sdf_spiral(vec2 uv, float width, float separation) {
    // base point
    float d = length(uv);
    // 8 arcs
    for (int i = 0; i < 8; i++) {
        uv.x -= width;
        if (uv.x < 0.0 && uv.y > 0.0)
            d = min(d, abs(length(uv) - width));
        uv.y -= width;
        uv = vec2(-uv.y, uv.x);
        width *= separation;
    }
    // tip point
    return min(d, length(uv));
}

/*************************************************
 *               SHAPE DRAWING API               *
 *************************************************/

float draw_shape_2d(vec2 uv, uint shape_type, float data0, 
	float data1, float data2, float data3, float data4, 
	float data5, float data6, float data7) {
	float sdf;
	if (shape_type == SHAPE_CIRCLE) // Draw circle shape
		sdf = sdf_circle(uv, data0);
	else if (shape_type == SHAPE_BOX) // Draw box shape
		sdf = sdf_box(uv, vec2(data0, data1));
	else if (shape_type == SHAPE_ROUNDED_BOX) // Draw rounded box shape
		sdf = sdf_rounded_box(
			uv, 
			vec2(data0, data1),
			vec4(data2, data3, data4, data5)
		);
	else if (shape_type == SHAPE_ORIENTED_BOX) // Draw oriented box shapes
		sdf = sdf_oriented_box(
			uv, 
			vec2(data0, data1),
			vec2(data2, data3), 
			data4
		);
	else if (shape_type == SHAPE_SEGMENT) // Draw segment shape
		sdf = sdf_segment(
			uv, 
			vec2(data0, data1),
			vec2(data2, data3)
		);
	else if (shape_type == SHAPE_THICK_SEGMENT) // Draw thick segment shape
		sdf = sdf_thick_segment(
			uv, 
			vec2(data0, data1),
			vec2(data2, data3), 
			data4
		);
	else if (shape_type == SHAPE_RHOMBUS) // Draw rhombus shape
		sdf = sdf_rhombus(uv, vec2(data0, data1));
	else if (shape_type == SHAPE_TRAPEZOID) // Draw trapezoid shape
		sdf = sdf_trapezoid(uv, data0, data1, data2);
	else if (shape_type == SHAPE_PARALLELOGRAM) // Draw parallelogram shape
		sdf = sdf_parallelogram(uv, data0, data1, data2);
	else if (shape_type == SHAPE_EQUILATERAL_TRIANGLE) // Draw equilateral triangle shape
		sdf = sdf_equilateral_triangle(uv, data0);
	else if (shape_type == SHAPE_ISOCELES_TRIANGLE) // Draw isoceles triangle shape
		sdf = sdf_isoceles_triangle(
			uv - vec2(data0, data1), 
			vec2(data2, data3)
		);
	else if (shape_type == SHAPE_TRIANGLE) // Draw triangle shape
		sdf = sdf_triangle(
			uv, 
			vec2(data0, data1),
			vec2(data2, data3),
			vec2(data4, data5)
		);
	else if (shape_type == SHAPE_UNEVEN_CAPSULE) // Draw uneven capsule shape
		sdf = sdf_uneven_capsule(uv, data0, data1, data2);
	else if (shape_type == SHAPE_PENTAGON) // Draw pentagon shape
		sdf = sdf_pentagon(uv, data0);
	else if (shape_type == SHAPE_HEXAGON) // Draw hexagon shape
		sdf = sdf_hexagon(uv, data0);
	else if (shape_type == SHAPE_NGON) // Draw ngon shape
		sdf = sdf_ngon(uv, data0, int(data1));
	else if (shape_type == SHAPE_STAR) // Draw star shape
		sdf = sdf_star(uv, data0, int(data1), data2);
	else if (shape_type == SHAPE_PIE) // Draw pie shape
		sdf = sdf_pie(uv, data0, data1);
	else if (shape_type == SHAPE_ARC) // Draw arc shape
		sdf = sdf_arc(uv, data0, data1, data2);
	else if (shape_type == SHAPE_HORSESHOE) // Draw horseshoe shape
		sdf = sdf_horseshoe(uv, data0, data1, data2, data3);
	else if (shape_type == SHAPE_VESICA) // Draw vesica shape
		sdf = sdf_vesica(uv, data0, data1);
	else if (shape_type == SHAPE_MOON) // Draw moon shape
		sdf = sdf_moon(uv, data0, data1, data2);
	else if (shape_type == SHAPE_ROUNDED_CROSS) // Draw rounded cross shape
		sdf = sdf_rounded_cross(uv, data0);
	else if (shape_type == SHAPE_EGG) // Draw egg shape
		sdf = sdf_egg(uv, data0, data1);
	else if (shape_type == SHAPE_HEART) // Draw heart shape
		sdf = sdf_heart(uv);
	else if (shape_type == SHAPE_CROSS) // Draw cross shape
		sdf = sdf_cross(uv, data0, data1, data2);
	else if (shape_type == SHAPE_ELLIPSE) // Draw ellipse shape
		sdf = sdf_ellipse(uv, vec2(data0, data1));
	else if (shape_type == SHAPE_PARABOLA) // Draw parabola shape
		sdf = sdf_parabola(uv, data0);
	else if (shape_type == SHAPE_PARABOLA_SEGMENT) // Draw parabola segment shape
		sdf = sdf_parabola_segment(uv, data0, data1);
	else if (shape_type == SHAPE_BEZIER) // Draw bezier shape
		sdf = sdf_bezier(
			uv, 
			vec2(data0, data1),
			vec2(data2, data3),
			vec2(data4, data5)
		);
	else if (shape_type == SHAPE_BLOBBY_CROSS) // Draw blobby cross shape
		sdf = sdf_blobby_cross(uv, data0, data1);
	else if (shape_type == SHAPE_TUNNEL) // Draw tunnel shape
		sdf = sdf_tunnel(uv, data0, data1);
	else if (shape_type == SHAPE_STAIRS) // Draw staris shape
		sdf = sdf_stairs(uv, vec2(data0, data1), data2);
	else if (shape_type == SHAPE_QUADRATIC_CIRCLE) // Draw quadratic circle shape
		sdf = sdf_quadratic_circle(uv);
	else if (shape_type == SHAPE_HYPERBOLA) // Draw hyperbola shape
		sdf = sdf_hyperbola(uv, data0, data1);
	else if (shape_type == SHAPE_COOL_S) // Draw cool S shape
		sdf = sdf_cool_s(uv);
	else if (shape_type == SHAPE_CIRCLE_WAVE) // Draw circle wave shape
		sdf = sdf_circle_wave(uv, data0, data1);
	else if (shape_type == SHAPE_ARROW) // Draw arrow shape
		sdf = sdf_arrow(
			uv, 
			vec2(data0, data1), 
			vec2(data2, data3), 
			data4, data5
		);
	else if (shape_type == SHAPE_CIRCLE_JOINT) // Draw circle joint shape
		sdf = sdf_circle_joint(uv, data0, data1, data2).x;
	else if (shape_type == SHAPE_FLAT_JOINT) // Draw flat joint shape
		sdf = sdf_flat_joint(uv, data0, data1, data2).x;
	else if (shape_type == SHAPE_HYPERBOLIC_CROSS) // Draw hyperbolic cross shape
		sdf = sdf_hyperbolic_cross(uv, data0);
	else if (shape_type == SHAPE_SPIRAL) // Draw spiral shape
		sdf = sdf_spiral(uv, data0, data1);
	return sdf;
}